# Aide et ressources de PyMca pour Synchrotron SOLEIL

[![](http://www.silx.org/doc/PyMca/dev/_static/PyMca_256x256.png)](http://www.silx.org/doc/PyMca/dev/)

## Résumé
- Visualisation des SPEC après conversion avec NXS2SPEC
- Open source

## Sources
- Code source: https://github.com/vasole/pymca
- Documentation officielle: http://www.silx.org/doc/PyMca/dev/

## Navigation rapide
| Fichiers téléchargeables | Tutoriaux | Page pan-data |
| ------ | ------ | ------ |
| [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/pymca-documentation/-/tree/master/documents) | [Tutoriaux officiels](http://www.silx.org/doc/PyMca/dev/tutorials.html) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/72/pymca) |
| [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/pymca-documentation/-/tree/master/dataset) | | |

## Installation
- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Facile (tout se passe bien)

## Format de données
- en entrée: NXS/SPEC
- en sortie:  
- Ruche locale
